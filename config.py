import os
import pymysql


basedir = os.path.abspath(os.path.dirname(__file__))
pymysql.install_as_MySQLdb()


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    DEBUG = True

    ### DB config ###

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'flogdb.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ### Email traceback ###

    MAIL_SERVER = 'smtp.fastmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = 'barni@pack.com'
    MAIL_PASSWORD = 'xrpv3s57qcp2shw8'
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_DEFAULT_SENDER = 'barni@pack.com'
    ADMINS = ['barni@pack.com']

    # MAIL_SERVER = os.environ.get('MAIL_SERVER')
    # MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    # MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    # MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    # MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    # ADMINS = ['barnitest12@gmail.com']

    ### Static folder ###

    STATIC_FOLDER = 'static'

    POSTS_PER_PAGE = 10

    ### Babel ###

    LANGUAGES = ['ru', 'en']

    ### Translator ###

    MS_TRANSLATOR_KEY = os.environ.get('MS_TRANSLATOR_KEY')

