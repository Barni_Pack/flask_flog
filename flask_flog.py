from app import app, db, cli
from app.models import User, Post


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}


if __name__ == '__main__':

    app.config.from_object('config')
    
    # STATIC_FOLDER == 
    app.static_url_path=app.config.get('STATIC_FOLDER')

    # app.root_path == .../flask_flog/app
    app.static_folder=app.root_path + '/' +  app.static_url_path

    print(app.static_folder)

    app.run(use_reloader=True)
